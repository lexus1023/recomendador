import pandas as pd

u_cols = ['user_id', 'edad', 'sexo', 'ocupacion', 'zip_code']
users = pd.read_csv('events/e.users', sep='|', names=u_cols,
 encoding='latin-1')

users = users[['user_id', 'edad', 'sexo', 'ocupacion']]##escojo solo las columnas que necesito

print users.shape
print users.head()

print users.ocupacion.unique()#tomo solo los que cumplen con los datos que necesito datos unicos


#Informacion items, eventos

#Reading items file:
i_cols = ['evento_id', 'evento_titulo' ,' date','video release date', 'IMDb URL', 'unknown', 'ArteUrbano', 'Cine',
 'Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura',
 'Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']
items = pd.read_csv('events/e.items', sep='|', names=i_cols,
 encoding='latin-1')

items = items[['evento_id', 'evento_titulo' ,' date', 'ArteUrbano', 'Cine','Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura','Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']]


print items.shape
print items.head()


#Informacion de peliculas asociadas .data
#Reading ratings file:
r_cols = ['user_id', 'event_id', 'rating', 'unix_timestamp']
ratings = pd.read_csv('events/e.data', sep='\t', names=r_cols, encoding='latin-1')

#Escogemos solo los que tenemos con nuestros datos
#http://pyciencia.blogspot.com/2015/05/obtener-y-filtrar-datos-de-un-dataframe.html

ratings = ratings[(ratings['user_id'] < 101) & (ratings['event_id'] < 51)]


print ratings.shape
print ratings.head()



###Acabamos de leer los datos que obtuvimos de los datasets
#https://towardsdatascience.com/train-test-split-and-cross-validation-in-python-80b61beca4b6
from sklearn.model_selection import train_test_split

ratings_base , ratings_test  = train_test_split(ratings, test_size=0.2)
print ratings_base.shape, ratings_test.shape


#Creamos un Sframe con graphlab
import graphlab
train_data = graphlab.SFrame(ratings_base)
test_data = graphlab.SFrame(ratings_test)




#Creamos un modelo de simple popularidad
popularity_model = graphlab.popularity_recommender.create(train_data, user_id='user_id', item_id='event_id', target='rating')


#Get recommendations for first 5 users and print them
#users = range(1,6) specifies user ID of first 5 users
#k=5 specifies top 5 recommendations to be given
#para usar listas https://devcode.la/tutoriales/listas-python/
lista_usuario = [1,3,5]
lista_usuario.append(6)
print lista_usuario
print "---------------------------------------------------------recomendaciones para los 5 primeros usuarios"
popularity_recomm = popularity_model.recommend(users=lista_usuario,k=5)
popularity_recomm.print_rows(num_rows=25)

print "---------------------------------------------------------recomendaciones medias mas altas en nuestro conjunto de datos"
print ratings_base.groupby(by='event_id')['rating'].mean().sort_values(ascending=False).head(20)




#Modelo de Filtrado colaborativo

#Train Model
item_sim_model = graphlab.item_similarity_recommender.create(train_data, user_id='user_id', item_id='event_id', target='rating', similarity_type='pearson')

#Coseno
item_sim_model_coseno = graphlab.item_similarity_recommender.create(train_data, user_id='user_id', item_id='event_id', target='rating', similarity_type='cosine')

#Make Recommendations:
item_sim_recomm = item_sim_model.recommend(users=range(1,6),k=5)
item_sim_recomm.print_rows(num_rows=25)
recomendacion=item_sim_recomm.to_dataframe()
print recomendacion.to_json(orient='records')


model_performance = graphlab.compare(test_data, [popularity_model, item_sim_model])
graphlab.show_comparison(model_performance,[popularity_model, item_sim_model])
graphlab.show_comparison(model_performance,[item_sim_model_coseno, item_sim_model])
#Evaluando recomendadores


