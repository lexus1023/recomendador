import pandas as pd
#Como usar json                 https://code.tutsplus.com/tutorials/how-to-work-with-json-data-using-python--cms-25758
#Como crear webservice
#github                         https://gist.github.com/miguelgrinberg/5614326
#pag                            https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
from flask import Flask,jsonify,json
# from flask.ext.httpauth import HTTPBasicAuth
from flask import abort
app = Flask(__name__, static_url_path = "")
# auth = HTTPBasicAuth()

# Leer los datos de usuario
# Leer datos de usuarios----------------------------------------------------------------------------------------------------------
u_cols = ['user_id', 'edad', 'sexo', 'ocupacion', 'zip_code']
users = pd.read_csv('C:/Users/Alexis/PycharmProjects/servidorFlask/events/e.users', sep='|', names=u_cols,
                    encoding='latin-1')
users = users[['user_id', 'edad', 'sexo', 'ocupacion']]  ##escojo solo las columnas que necesito
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
newt=users
cont=100
tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'M ilk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]


@app.route('/recommender/api/v1.0/events', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})





#--------------------------POST------------------------------------------------------------------------------------
from flask import request
@app.route('/recommender/api/v1.0/events/', methods=['POST'])
def create_task():
    print users
    content = request.get_json()
    task = {

        'user_id': users.iloc[-1,0] + 1,
        # 'user_id': cont ,
        'edad': content['edad'],
        'sexo': content['sexo'],
        'ocupacion': content['ocupacion']
    }

    df = pd.DataFrame({'user_id':[users.iloc[-1,0] + 1],'edad':[content['edad']],'sexo': content['sexo'],'ocupacion':content['ocupacion']})
    print df
    print task

    newt =pd.concat([users, df])

    print newt
    return 'Json Bien'

#--------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------
@app.route('/recommender/api/v1.0/events/<int:task_id>', methods=['GET'])
def get_task(task_id):
    # -------------------------------------------------------analisis-------------------------------------------------------

    # #Leer datos de usuarios
    # u_cols = ['user_id', 'edad', 'sexo', 'ocupacion', 'zip_code']
    # users = pd.read_csv('C:/Users/Alexis/PycharmProjects/servidorFlask/events/e.users', sep='|', names=u_cols, encoding='latin-1')
    #
    # users = users[['user_id', 'edad', 'sexo', 'ocupacion']]  ##escojo solo las columnas que necesito

    # Leer datos de items

    i_cols = ['event_id', 'evento_titulo', 'fecha', 'video release date', 'IMDb URL', 'unknown', 'ArteUrbano', 'Cine',
              'Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura',
              'ArtesEscenicas', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales',
              'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']
    items = pd.read_csv('C:/Users/Alexis/PycharmProjects/servidorFlask/events/e.items', sep='|', names=i_cols,
                        encoding='latin-1')

    items = items[
        ['event_id', 'evento_titulo', 'fecha', 'ArteUrbano', 'Cine', 'Danza', 'Exposiciones', 'Gratis', 'Museos',
         'Teatro', 'Talleres', 'Literatura', 'ArtesEscenicas', 'FestivalFerias', 'ForosConferencias', 'Deportes',
         'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']]

    # Leer datos de ratings

    r_cols = ['user_id', 'event_id', 'rating', 'unix_timestamp']
    ratings = pd.read_csv('C:/Users/Alexis/PycharmProjects/servidorFlask/events/e.data', sep='\t', names=r_cols,
                          encoding='latin-1')

    # Escogemos solo los que tenemos con nuestros datos
    # http://pyciencia.blogspot.com/2015/05/obtener-y-filtrar-datos-de-un-dataframe.html

    ratings = ratings[(ratings['user_id'] < 101) & (ratings['event_id'] < 51)]

    # -----------------------------------------------------------------
    from sklearn.model_selection import train_test_split

    ratings_base, ratings_test = train_test_split(ratings, test_size=0.2)
    print ratings_base.shape, ratings_test.shape

    # Creamos un Sframe con graphlab
    import graphlab
    train_data = graphlab.SFrame(ratings_base)
    test_data = graphlab.SFrame(ratings_test)

    # Creamos un modelo de simple popularidad
    popularity_model = graphlab.popularity_recommender.create(train_data, user_id='user_id', item_id='event_id',
                                                              target='rating')
    lista_usuario = []
    lista_usuario.append(task_id)
    print lista_usuario
    print '*******************************************'
    print(newt)
    popularity_recomm = popularity_model.recommend(users=lista_usuario,k=2)
    # popularity_recomm = popularity_model.recommend(newt=lista_usuario, k=2)

    items_recomendados = popularity_recomm.to_dataframe()

    eventos_recomendados = pd.DataFrame(items_recomendados, columns=['event_id'])
    print eventos_recomendados
    # Recorrer el dataframe
    for index in eventos_recomendados.iterrows():
        print index[1]
    # Pasamos a lista
    eventos_recomendados = eventos_recomendados['event_id'].tolist()

    print eventos_recomendados
    #
    # eventos_recomendados= eventos_recomendados.loc[eventos_recomendados , ['event_id', 'evento_titulo' ,' date','video release date', 'IMDb URL', 'unknown', 'ArteUrbano', 'Cine',
    # 'Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura',
    # 'Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']]

    # eventos_filtrados=items[items['event_id']==eventos_recomendados]

    # print eventos_filtrados
    # eventos_filtrados = [eventos_filtrados for eventos_filtrados in items if items['event_id'] == task_id]
    eventos_filtrados = []
    print items["fecha"][3]
    for item in eventos_recomendados:
        # eventos_filtrados.append(eventos_recomendados[eventos_recomendados[]])
        # eventos_filtrados.append(items[(items['event_id'] == item)])
        print item
        # if  item is items['event_id']:
        eventoDict = {
            # 'event_id':items[(items['event_id'] == item)],
            # 'event_id':items[(items['event_id'] == item)],
            "event_id": items['event_id'][item - 1],
            "evento_titulo": items['evento_titulo'][item - 1],
            "fecha": items["fecha"][item - 1]
            #     ,
            # "categoria":[
            #     {
            #         "ArteUrbano":items["ArteUrbano"][item - 1],
            #         "Cine":items["Cine"][item - 1],
            #         "Danza":items["Danza"][item - 1],
            #         "Exposiciones":items["Exposiciones"][item - 1],
            #         "Gratis":items["Gratis"][item - 1],
            #         "Museos":items["Museos"][item - 1],
            #         "Teatro":items["Teatro"][item - 1],
            #         "Talleres":items["Talleres"][item - 1],
            #         "Literatura":items["Literatura"][item - 1],
            #         "ArtesEscenicas":items["ArtesEscenicas"][item - 1],
            #         "Deportes":items["Deportes"][item - 1],
            #         "FestivalFerias":items["FestivalFerias"][item - 1],
            #         "ForosConferencias":items["ForosConferencias"][item - 1],
            #         "ArtesVisuales":items["ArtesVisuales"][item - 1],
            #         "EvMunicipalCumanda":items["EvMunicipalCumanda"][item - 1],
            #         "CCulturalMetropolitano":items["CCulturalMetropolitano"][item - 1],
            #         "QuitoTurismo":items["QuitoTurismo"][item - 1],
            #         "Convocatoria":items["Convocatoria"][item - 1]
            #
            #     }
            # ]
            # ['event_id', 'evento_titulo', 'fecha', 'ArteUrbano', 'Cine', 'Danza', 'Exposiciones', 'Gratis', 'Museos',
            #  'Teatro', 'Talleres', 'Literatura', 'Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes',
            #  'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria']]

            # 'evento_titulo', ' date', 'video release date', 'IMDb URL', 'unknown', 'ArteUrbano', 'Cine',
            # 'Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura',
            # 'Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria'
        }
        eventos_filtrados.append(eventoDict)

        # if item in items:
        #     eventos_filtrados.append(items[items['event_id'] == item['event_id']])

    # for para json
    for item_evento_recomendado in eventos_recomendados:
        if item_evento_recomendado is items:
            eventoDict = {
                # 'event_id':items[(items['event_id'] == item_evento_recomendado  ],
                # 'evento_titulo', ' date', 'video release date', 'IMDb URL', 'unknown', 'ArteUrbano', 'Cine',
                # 'Danza', 'Exposiciones', 'Gratis', 'Museos', 'Teatro', 'Talleres', 'Literatura',
                # 'Deportes', 'FestivalFerias', 'ForosConferencias', 'Deportes', 'ArtesVisuales', 'EvMunicipalCumanda', 'CCulturalMetropolitano', 'QuitoTurismo', 'Convocatoria'
            }

    # print eventos_filtrados[1]
    #
    # eventos_filtrados= json.dumps(eventos_filtrados)
    df = pd.DataFrame.from_dict(eventos_filtrados, orient='columns')
    print df.to_json(orient='records')

    print   eventos_filtrados

    # falta pasar a json el item
    # --------------------------------------------------------------------------------------------------------------
    return df.to_json(orient='records')
    # return json.dumps(eventos_filtrados)
#--------------------------------------------------------------------------------------------------------------



@app.route('/')
def hello_world():
    return 'Hello Hmia!'


if __name__ == '__main__':
    app.run()
