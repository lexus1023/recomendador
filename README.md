# Explicación De Archivos Trabajo Minería

- Folder analysis
    -   Presentamos el analisis de los datos que hemos definido, para poder elegir el recomendador, y cual es la exactitud

-   Folder application
    -   Tenemos la aplicación Cliente en su totalidad.

-   Folder mineriadata      -   tenemos toda la data de minería, que hemos usado para poder realizar los análisis

-   Folder server
    -   tenemos el servidor funcional y su respectiva lógica ya en un ambiente de pruebas.

-   Folder manuales
    -   Manual de minería
    -   Manual Técnico
    -   Manual de Usuario